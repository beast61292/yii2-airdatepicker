<?php

namespace airdatepicker;

use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\InputWidget;

/**
 * Class AirDatePicker
 * Manual http://t1m0n.name/air-datepicker/docs/index-ru.html
 * GitHub https://github.com/t1m0n/air-datepicker
 * @package mrserg161\airdatepicker
 * @author Malovichko Sergey <mrSerg161@gmail.com>
 */

class DatePicker extends InputWidget
{
    public $template = '{input}';

    public $options = [
        'class' => 'form-control',
    ];
    public $clientOptions = [];
    public $clientEvents = [];

    public $dateFormat;

    public function init()
    {
        parent::init();
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->hasModel() ? Html::getInputId($this->model, $this->attribute) : $this->getId();
        }
        if ($this->dateFormat === null) {
            $this->dateFormat = Yii::$app->formatter->datetimeFormat;
        }
    }

    public function run()
    {
        $asset = DatePickerAsset::register($this->view);

        if (isset($this->clientOptions['language'])) {
            $lang = $this->clientOptions['language'];
            if (file_exists("files/locale/$lang.js"))
            {
                $this->view->registerJsFile("/files/locale/$lang.js", [
                    'depends' => DatePickerAsset::class
                ]);
            }
            else{
                $this->view->registerJsFile("/files/locale/en.js", [
                    'depends' => DatePickerAsset::class
                ]);
            }
        }

        // get formatted date value
        if ($this->hasModel()) {
            $value = Html::getAttributeValue($this->model, $this->attribute);
        } else {
            $value = $this->value;
        }
        if ($value !== null && $value !== '') {
            try {
                if (is_int($value)) {
                    $value = Yii::$app->formatter->asDatetime($value, $this->dateFormat);
                }
            } catch (InvalidParamException $e) {
                // ignore exception and keep original value if it is not a valid date
            }
        }

        $id = $this->options['id'];
        $js = "new AirDatepicker('#$id', {";


        foreach ($this->clientOptions as $option => $valueOption)
        {
            if (is_bool($valueOption))
            {
                $params[] = $option.':' . ($valueOption?'true':'false');
            }
            else
            {
                $params[] = "{$option} :  '{$valueOption}'";
            }
        }

        if ($value) {
            if (is_array($value))
            {
                $this->clientEvents = array_merge($this->clientEvents, ['selectDate' => '[new Date(' . strtotime($value[0]) * 1000 . '), new Date(' . strtotime($value[1]) * 1000 . ')]']);
                $value = $value[0];
            }
            else
            {
                $this->clientEvents = array_merge($this->clientEvents, ['selectDate' => 'new Date(' . strtotime($value) * 1000 . ')']);
            }
        }
        foreach ($this->clientEvents as $event => $handler) {
            $params[] = $event.':'.$handler;
        }

        $js .= implode(',', $params);
        $js .= "})";

        $this->view->registerJs($js . ';');

        $this->options['value'] = $value;

        return strtr($this->template, [
            '{input}' => $this->hasModel()
                ? Html::activeTextInput($this->model, $this->attribute, $this->options)
                : Html::textInput($this->name, $value, $this->options),
        ]);
    }
}
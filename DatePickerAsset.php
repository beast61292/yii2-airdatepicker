<?php

namespace airdatepicker;

use yii\bootstrap4\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class DatePickerAsset extends AssetBundle
{
    public $sourcePath = '@bower/air-datepicker/dist';

    public $css = [
        'air-datepicker.css',
    ];

    public $js = [
        'air-datepicker.js',
    ];

    public $depends = [
        JqueryAsset::class,
        BootstrapAsset::class,
    ];
}